USE [appsure_integrity]

GO
insert tblLUTState (fname) values ('ACT')
insert tblLUTState (fname) values ('NSW')
insert tblLUTState (fname) values ('NT')
insert tblLUTState (fname) values ('QLD')
insert tblLUTState (fname) values ('SA')
insert tblLUTState (fname) values ('TAS')
insert tblLUTState (fname) values ('VIC')
insert tblLUTState (fname) values ('WA')

GO
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (1, N'ACT', N'Australian Capital Territory')
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (2, N'NSW', N'New South Whales')
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (3, N'NT', N'Northern Territory')
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (4, N'QLD', N'Queensland')
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (5, N'SA', N'South Australia')
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (6, N'TAS', N'Tasmania')
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (7, N'VIC', N'Victoria')
INSERT [Lookup].[States] ([Id], [Code], [Nane]) VALUES (8, N'WA', N'Western Australia')

GO
insert config.IntermediaryType (Name) values ('Dealer')